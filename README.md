*Rules of the Game*

1. Each player has his or her own deck of cards.
2. Each player draws twenty cards from his or her deck.
3. Each player also starts with a max hit point count of 800pts. 
4. The game starts with the flip of a coin to decide who goes first.
5. Players alternate turns.

   * A turn consists of three phases.
      * The Defensive phase.
         * player chooses up to two cards from their hand to play as defensive cards. This can be any card that they have available but it's offensive capabilities will have no effect that round. 
         * The defensive points for all cards the player currently has placed in a defensive position are added up to make a defensive total for that round.  

      * The Offensive phase.
         * player chooses up to two cards from their hand to play as offensive cards. This can be any card they have available but it's defensive capabilities will have no effect.
         * The offensive points for all cards the player currently has placed in an offensive position are added up to make an offensive total for that round. 
      * The magic phase.
         * Magic cards are specific cards within a players deck that grants the player special abilities. These are abilities added to previously played cards or overarching weather cards. 
         * Ability cards are placed next to or on top of offensive or defensive cards that have already been played. Each card has a set amount of ability slots and only an equivalent amount of ability cards can be played on top of that card. 
         * Weather cards are overarching cards that affect certain offensive or defensive cards.
         * Special cards allow the player to either look into the opponent's hand or deck, take a card from their opponent's graveyard, draw a two cards from their own deck, or revive one card from their own graveyard. 


6. Once each player has made a turn the round is over and all defensive and offensive points are added up for both players. Those totals are compared to each other and the players hit points are adjusted accordingly. 

7. Rounds continue until one player is depleted to zero hit points. The opposing player wins.