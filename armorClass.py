#Sam LeCompte
#Final Project Class 

class Armor:

    def __init__(self, head, chest, hands, legs, speed):
        self.__head = head
        self.__chest = chest
        self.__hands = hands
        self.__legs = legs
        self.__speed = speed

    def get_head(self):
        return self.__head

    def get_chest(self):
        return self.__chest

    def get_hands(self):
        return self.__hands

    def get_legs(self):
        return self.__legs

    def get_speed(self):
        return self.__speed

    def showArmor(self):
        return "Head: " +  str(self.get_head()) + " Chest: " + str(self.get_chest())

