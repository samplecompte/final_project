#Sam LeCompte
#Final Project

class Character:
    def __init__(self, armor, attack1, attack2, attackSpec, weapon, magicSlot1, magicSlot2, magicSlot3):
        self.__armor = armor
        self.__attack1 = attack1
        self.__attack2 = attack2
        self.__attackSpec = attackSpec
        self.__weapon = weapon
        self.__magicSlot1 = magicSlot1
        self.__magicSlot2 = magicSlot2
        self.__magicSlot3 = magicSlot3

    def get_armor(self):
        return self.__armor

    def get_attack1(self):
        return self.__attack1

    def get_attack2(self):
        return self.__attack2

    def get_attackSpec(self):
        return self.__attackSpec

    def get_weapon(self):
        return self.__weapon

    def get_magicSlot1(self):
        return self.__magicSlot1

    def get_magicSlot2(self):
        return self.__magicSlot2

    def get_magicSlot3(self):
        return self.__magicSlot3

    def showCharacter(self):
        print("Armor: " + str(self.__armor.showArmor()) + "\n"
              "Attack1: " + str(self.__attack1) + "\n"
              "Attack2: " + str(self.__attack2) + "\n"
              "Special Attack: " + str(self.__attackSpec) + "\n"
              "Weapon: " + str(self.__weapon) + "\n"
              "Magic Slot1: " + str(self.__magicSlot1) + "\n"
              "Magic Slot2: " + str(self.__magicSlot2) + "\n"
              "Magic Slot3: " + str(self.__magicSlot3))
