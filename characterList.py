from armorClass import *
from martialAttacks import *
from weaponClass import *
from magicClass import *
from characterClass import *

#WeaponList
sword = Weapon(75,75,30,2,50,0)
axe = Weapon(40,65,80,2,45,0)
mace = Weapon(15,15,90,2,60,0)
bow = Weapon(90,0,0,70,15,0)
staff = Weapon(0,0,60,40,20,80)
sceptre = Weapon(0,0,50,50,15,85)
spear = Weapon(75,40,10,40,25,0)
hammer = Weapon(20,0,90,2,45,0)
dagger = Weapon(85,65,15,1,25,0)
brassKnuckles = Weapon(15,5,75,1,40,0)
sword_of_Elvish_Decent = Weapon(85,85,40,2,60,15)
hammer_of_Dwarfish_Decent = Weapon(30,10,100,2,55,15)
staff_of_the_Pheonix = Weapon(0,10,70,50,30,100)
master_Theives_Knuckles = Weapon(25,15,90,1,50,25)
sceptre_of_Death = Weapon(0,0,70,25,20,90)
axe_of_peril = Weapon(50,70,90,2,45,15)
bow_of_the_master_archer = Weapon(100,0,0,90,20,30)
dagger_of_bloodletting = Weapon(75,50,25,2,45,30)
spear_of_bones = Weapon(90,20,20,60,25,30)

#ArmorList

hide = Armor(0,65,40,65,70)
steel_plate = Armor(70,70,70,70,15)
chain_mail = Armor(45,70,45,70,45)
scale_mail = Armor(60,70,50,70,30)
robe = Armor(0,40,25,40,100)
silver_plate = Armor(70,70,70,60,30)
magic_aurora = Armor(50,50,50,50,100)

#MartialAttacks

right_punch = MartialAttacks(70,0,0,15)
left_punch = MartialAttacks(70,0,0,15)
right_kick = MartialAttacks(0,80,0,0)
left_kick = MartialAttacks(0,80,0,0)
headbutt = MartialAttacks(20,0,0,80)
knee_of_fury = MartialAttacks(0,10,80,10)
flurry_of_fists = MartialAttacks(90,0,0,0)
wooshi_finger_hold = MartialAttacks(80,0,0,50)
the_great_kings_execution = MartialAttacks(60,40,50,0)
thousand_kicks = MartialAttacks(0,90,10,0)

#MagicAttacks

breath_of_fire = Magic(70,0,20,0,0,0)
waterfall = Magic(0,70,0,20,0,0)
surge_of_the_earth = Magic(0,0,0,70,0,0)
lighting_bolt = Magic(10,0,10,0,80,0)
forest_growth = Magic(0,10,10,60,10,0)
being_of_the_wind = Magic(0,0,80,0,10,0)
touch_of_death = Magic(0,0,10,0,10,80)
control = Magic(0,0,0,0,0,90)
birth_of_embers = Magic(90,0,10,0,0,0)
tidal_wave = Magic(0,90,0,10,0,0)
song_of_sleep = Magic(0,15,40,0,0,90)
holy_decent = Magic(0,25,65,5,80,0)

#Characters

knomeish_barbarian = Character(hide, right_punch, left_punch, knee_of_fury, hammer, forest_growth, 0, 0)
knomeish_bard = Character(hide, right_punch, left_kick, 0, dagger, control, birth_of_embers, 0)
knomeish_cleric = Character(steel_plate, left_punch, headbutt, the_great_kings_execution, sword, lighting_bolt, forest_growth, 0)
knomeish_druid = Character(hide, 0, 0, 0, sceptre, forest_growth, waterfall, being_of_the_wind)
knomeish_fighter = Character(silver_plate, right_punch, left_punch, the_great_kings_execution, sword, 0, 0, 0)
knomeish_monk = Character(robe, right_kick, left_kick, flurry_of_fists, brassKnuckles, being_of_the_wind, tidal_wave, 0)
knomeish_paladin = Character(steel_plate, right_punch, left_punch, the_great_kings_execution, mace, lighting_bolt, 0, 0)
knomeish_ranger = Character(hide, right_kick, left_punch, wooshi_finger_hold, bow, forest_growth, 0, 0)
knomeish_rogue = Character(hide, right_punch, headbutt, thousand_kicks, dagger, control, 0, 0)
knomeish_sorcerer = Character(magic_aurora, right_punch, left_punch, flurry_of_fists, 0, breath_of_fire, lighting_bolt, surge_of_the_earth)
knomeish_warlock = Character(chain_mail, 0, 0, knee_of_fury, spear, forest_growth, surge_of_the_earth, 0)
knomeish_wizard = Character(robe, 0, 0, wooshi_finger_hold, staff, lighting_bolt, waterfall, touch_of_death)
Gendor_the_legandary_knomeish_rogue = Character(hide, right_punch, flurry_of_fists, thousand_kicks, master_Theives_Knuckles, control, touch_of_death, 0)

halfling_barbarian = Character(chain_mail, right_punch, headbutt, knee_of_fury, axe, birth_of_embers, 0, 0)
halfling_bard = Character(hide, right_punch, left_punch, 0, sword, forest_growth, being_of_the_wind, 0)
halfling_cleric = Character(steel_plate, right_punch, right_kick, knee_of_fury, hammer, lighting_bolt, forest_growth, 0)
halfling_druid = Character(magic_aurora, 0,0,0,sceptre, forest_growth, waterfall, tidal_wave)
halfling_fighter = Character(chain_mail, right_punch, left_kick, the_great_kings_execution, sword, surge_of_the_earth, 0, 0)
halfling_monk = Character(hide, headbutt, right_punch, thousand_kicks,brassKnuckles, being_of_the_wind, lighting_bolt, 0)
halfling_paladin = Character(silver_plate, right_punch, left_punch, headbutt, spear, surge_of_the_earth, control, 0,)
halfling_ranger = Character(hide, left_punch, left_kick, wooshi_finger_hold, bow, forest_growth, 0, 0)
halfling_rogue = Character(hide, right_kick, left_kick, flurry_of_fists, dagger, control, touch_of_death, 0)
halfling_sorcerer = Character(magic_aurora, right_punch, left_punch, wooshi_finger_hold, 0, waterfall, birth_of_embers, being_of_the_wind)
halfling_warlock = Character(scale_mail, left_kick, right_punch, 0, dagger, forest_growth, waterfall, control)
halfling_wizard = Character(robe, right_punch, right_kick, headbutt, staff, breath_of_fire, lighting_bolt, tidal_wave)
Leif_the_singing_halfling = Character(silver_plate, right_punch, headbutt, wooshi_finger_hold, dagger_of_bloodletting, control, surge_of_the_earth, song_of_sleep)

dwarfish_barbarian = Character(hide, right_punch, left_punch, headbutt, axe, birth_of_embers, 0, 0)
dwarfish_bard = Character(hide, right_punch, left_kick, knee_of_fury, dagger, control, tidal_wave, surge_of_the_earth)
dwarfish_cleric = Character(scale_mail, left_punch, right_kick, flurry_of_fists, hammer, breath_of_fire, lighting_bolt, 0)
dwarfish_druid = Character(hide, 0,0,0, sceptre, forest_growth, tidal_wave, waterfall)
dwarfish_fighter = Character(steel_plate, right_punch, left_punch, headbutt, sword, surge_of_the_earth, 0,0)
dwarfish_monk = Character(hide, right_punch, left_punch, headbutt, 0, surge_of_the_earth, birth_of_embers, being_of_the_wind)
dwarfish_paladin = Character(silver_plate, left_punch, right_kick, the_great_kings_execution, spear, surge_of_the_earth, lighting_bolt, 0)
dwarfish_ranger = Character(hide, left_kick, right_kick, 0, bow, forest_growth, waterfall, control)
dwarfish_rogue = Character(hide, right_punch, left_punch, flurry_of_fists,dagger, control, 0,0)
dwarfish_sorcerer = Character(magic_aurora, left_kick, right_kick, 0, 0, breath_of_fire, waterfall, lighting_bolt)
dwarfish_warlock = Character(chain_mail, left_punch, right_punch, 0, spear, forest_growth, control, birth_of_embers)
dwarfish_wizard = Character(robe, 0, 0, thousand_kicks, staff, birth_of_embers, tidal_wave, lighting_bolt)
Falmouth_the_Holy_Dwarf = Character(silver_plate, left_punch, right_punch, the_great_kings_execution, hammer_of_Dwarfish_Decent, lighting_bolt, holy_decent, forest_growth)

elfish_barbarian = Character(scale_mail, left_punch, right_punch, headbutt, sword, forest_growth,surge_of_the_earth, 0)
elfish_bard = Character(chain_mail, left_kick, right_punch, knee_of_fury, hammer, control, 0, 0)
elfish_cleric = Character(silver_plate, left_punch, right_punch, the_great_kings_execution, mace, lighting_bolt, forest_growth, 0)
elfish_druid = Character(hide, 0,0,0, sceptre, forest_growth, tidal_wave, lighting_bolt)
elfish_fighter = Character(steel_plate, right_kick, right_punch, flurry_of_fists, sword, surge_of_the_earth, 0,0)
elfish_monk = Character(0, right_kick, left_kick, flurry_of_fists,0, surge_of_the_earth, lighting_bolt, being_of_the_wind)
elfish_paladin = Character(steel_plate, right_punch, left_punch, knee_of_fury, axe, surge_of_the_earth, forest_growth, 0)
elfish_ranger = Character(hide, right_kick, left_kick, 0, bow, forest_growth, waterfall, 0)
elfish_rogue = Character(hide, right_punch, left_kick, headbutt, dagger, control, touch_of_death, 0)
elfish_sorcerer = Character(magic_aurora, right_punch, left_punch, thousand_kicks, 0, breath_of_fire, birth_of_embers, lighting_bolt)
elfish_warlock = Character(hide, right_punch, left_punch, 0, sword, forest_growth, being_of_the_wind, lighting_bolt)
elfish_wizard = Character(robe, right_punch, left_punch, 0, staff, birth_of_embers, lighting_bolt, waterfall)
Mitherel_the_great_Wizard = Character(robe, right_kick, left_kick, the_great_kings_execution, staff_of_the_Pheonix, birth_of_embers, breath_of_fire, lighting_bolt)

half_elf_barbarian = Character(hide, right_punch, left_punch, headbutt, axe, surge_of_the_earth, 0,0)
half_elf_bard = Character(chain_mail, left_kick, right_kick, flurry_of_fists, dagger, control, 0,0)
half_elf_cleric = Character(steel_plate, right_punch, left_punch, knee_of_fury, hammer, lighting_bolt, waterfall, 0)
half_elf_druid = Character(hide, 0,0,0, sceptre, surge_of_the_earth, birth_of_embers, forest_growth)
half_elf_fighter = Character(silver_plate, right_punch, left_punch, headbutt, sword, forest_growth, waterfall, 0,)
half_elf_monk = Character(0, right_punch, left_punch, thousand_kicks, brassKnuckles, surge_of_the_earth, being_of_the_wind, forest_growth)
half_elf_paladin = Character(silver_plate, right_punch, left_punch, the_great_kings_execution, spear, lighting_bolt, tidal_wave, 0)
half_elf_ranger = Character(hide, right_kick, left_kick, flurry_of_fists, bow, forest_growth, 0, 0)
half_elf_rogue = Character(hide, right_punch, left_kick, thousand_kicks, dagger, control, 0, 0)
half_elf_sorcerer = Character(magic_aurora, left_kick, right_kick, 0, 0, breath_of_fire, lighting_bolt, tidal_wave)
half_elf_warlock = Character(hide, right_kick, left_punch, 0, sceptre, forest_growth, lighting_bolt, waterfall)
half_elf_wizard = Character(robe, right_punch, left_punch, headbutt, staff, breath_of_fire, tidal_wave, control)
Hondor_the_half_elf_king = Character(silver_plate, right_punch, right_kick, the_great_kings_execution, sword_of_Elvish_Decent, surge_of_the_earth, lighting_bolt, forest_growth)

tiefling_barbarian = Character(hide, right_punch, left_punch, headbutt, axe, control, touch_of_death, 0)
tiefling_bard = Character(hide, left_kick, right_kick, knee_of_fury, hammer, control, touch_of_death, 0)
tiefling_cleric = Character(steel_plate, right_punch, left_punch, the_great_kings_execution, spear, lighting_bolt, control, touch_of_death)
tiefling_druid = Character(chain_mail, 0, 0, 0, sceptre, forest_growth, tidal_wave, touch_of_death)
tiefling_fighter = Character(steel_plate, right_punch, left_punch, thousand_kicks, sword, surge_of_the_earth, touch_of_death, 0)
tiefling_monk = Character(hide, right_kick, left_kick, flurry_of_fists, brassKnuckles, being_of_the_wind, control, touch_of_death)
tiefling_paladin = Character(silver_plate, right_punch, left_punch, knee_of_fury, mace, surge_of_the_earth, touch_of_death, lighting_bolt)
tiefling_ranger = Character(hide, right_kick, left_kick, thousand_kicks, bow, forest_growth, touch_of_death, 0)
tiefling_rogue = Character(hide, right_punch, left_kick, headbutt, dagger, control, touch_of_death, birth_of_embers)
tiefling_sorcerer = Character(magic_aurora, right_punch, left_punch, 0, 0, breath_of_fire, birth_of_embers, touch_of_death)
tiefling_warlock = Character(hide, right_punch, left_punch, flurry_of_fists, sceptre, forest_growth, touch_of_death, 0)
tiefling_wizard = Character(robe, right_kick, 0, 0, staff, lighting_bolt, tidal_wave, touch_of_death)
Shotu_the_bearer_of_death = Character(hide, right_punch, left_punch, wooshi_finger_hold, sceptre_of_Death, control, touch_of_death, breath_of_fire)

human_barbarian = Character(scale_mail, right_punch, left_punch, headbutt, axe, surge_of_the_earth, 0,0)
human_bard = Character(hide, right_punch, left_kick, knee_of_fury, dagger, control, touch_of_death, 0)
human_cleric = Character(silver_plate, right_punch, left_punch, the_great_kings_execution, hammer, lighting_bolt, forest_growth, 0)
human_druid = Character(hide, 0, 0, 0, spear, forest_growth, tidal_wave, waterfall)
human_fighter = Character(steel_plate, right_punch, left_punch, knee_of_fury, sword, surge_of_the_earth, forest_growth, 0)
human_monk = Character(hide, right_kick, left_kick, flurry_of_fists, brassKnuckles, being_of_the_wind, breath_of_fire, forest_growth)
human_paladin = Character(silver_plate, right_punch, left_punch, headbutt, mace, lighting_bolt, forest_growth, 0)
human_ranger = Character(hide, right_kick, left_kick, thousand_kicks, bow, forest_growth, 0, 0)
human_rogue = Character(hide, right_punch, left_kick, flurry_of_fists, dagger, control, touch_of_death, 0)
human_sorcerer = Character(magic_aurora, right_kick, left_punch, headbutt, 0, breath_of_fire, birth_of_embers, lighting_bolt)
human_warlock = Character(hide, right_punch, left_punch, 0, sceptre, forest_growth, tidal_wave, control)
human_wizard = Character(robe, right_kick, left_kick, 0, staff, lighting_bolt, tidal_wave, birth_of_embers)
Thomas_the_knight_of_the_sun = Character(scale_mail, right_punch, left_kick, the_great_kings_execution, spear_of_bones, birth_of_embers, breath_of_fire, lighting_bolt)

dragonborn_barbarian = Character(steel_plate, right_punch, left_punch, headbutt, hammer, surge_of_the_earth, 0,0)
dragonborn_bard = Character(hide, right_punch, left_kick, knee_of_fury, dagger, control, forest_growth, 0)
dragonborn_cleric = Character(silver_plate, right_punch, left_punch, the_great_kings_execution, hammer, lighting_bolt, surge_of_the_earth, 0)
dragonborn_druid = Character(hide, 0, 0,0, spear, forest_growth, surge_of_the_earth, tidal_wave)
dragonborn_fighter = Character(scale_mail, right_kick, left_kick, thousand_kicks, sword, breath_of_fire, surge_of_the_earth, 0)
dragonborn_monk = Character(hide, right_punch, flurry_of_fists, thousand_kicks, brassKnuckles, being_of_the_wind,0,0)
dragonborn_paladin = Character(steel_plate, right_punch, left_punch, headbutt, axe, lighting_bolt, surge_of_the_earth, 0)
dragonborn_ranger = Character(hide, right_kick, left_kick, thousand_kicks, bow, forest_growth, 0,0)
dragonborn_rogue = Character(hide, right_punch, left_kick, wooshi_finger_hold, dagger, control, touch_of_death, 0)
dragonborn_sorcerer = Character(magic_aurora, right_kick, left_kick, 0, 0, breath_of_fire, birth_of_embers, waterfall)
dragonborn_warlock = Character(hide, right_punch, left_punch, knee_of_fury, sceptre, forest_growth, control, 0)
dragonborn_wizard = Character(robe, right_kick, left_punch, wooshi_finger_hold, staff, breath_of_fire, lighting_bolt, tidal_wave)
Drefor_the_great_hunter =(scale_mail, right_kick, left_kick, thousand_kicks, bow_of_the_master_archer, forest_growth, surge_of_the_earth, control)

half_orc_barbarian = Character(chain_mail, right_punch, left_punch, headbutt, axe, surge_of_the_earth, 0,0)
half_orc_bard = Character(hide, right_kick, right_punch, knee_of_fury, dagger, control, forest_growth, 0)
half_orc_cleric = Character(scale_mail, right_punch, left_punch, the_great_kings_execution, hammer, lighting_bolt, surge_of_the_earth,0)
half_orc_druid = Character(hide, 0,0,0, spear, forest_growth, surge_of_the_earth, tidal_wave)
half_orc_fighter = Character(steel_plate, right_punch, left_punch, knee_of_fury, sword, surge_of_the_earth, touch_of_death, 0)
half_orc_monk = Character(hide, right_punch, flurry_of_fists, thousand_kicks, 0, being_of_the_wind, surge_of_the_earth, 0)
half_orc_paladin = Character(silver_plate, right_punch, left_punch, headbutt, spear, surge_of_the_earth, lighting_bolt, 0)
half_orc_ranger = Character(hide, right_kick, left_kick, knee_of_fury, bow, forest_growth, breath_of_fire, 0)
half_orc_rogue = Character(hide, right_punch, left_punch, headbutt, dagger, control, touch_of_death, forest_growth)
half_orc_sorcerer = Character(magic_aurora, right_kick, left_punch, 0, 0, breath_of_fire, birth_of_embers, lighting_bolt)
half_orc_warlock = Character(chain_mail, right_punch, left_punch, knee_of_fury, sceptre, forest_growth, waterfall, control)
half_orc_wizard = Character(robe, right_kick, left_kick, the_great_kings_execution, staff, lighting_bolt, tidal_wave, 0)
Olgorf_the_Destroyer = Character(scale_mail, right_kick, headbutt, knee_of_fury, axe_of_peril, surge_of_the_earth, breath_of_fire, 0)

charList = [
    half_orc_barbarian, half_orc_bard, half_orc_cleric,
]













































