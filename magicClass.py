#Sam LeCompte
#12/17/15
#Final Project

class Magic:
    def __init__(self, fire, water, air, earth, lighting, darkness):
        self.__fire = fire
        self.__water = water
        self.__air = air
        self.__earth = earth
        self.__lighting = lighting
        self.__darkness = darkness

    def get_fire(self):
        return self.__fire

    def get_water(self):
        return self.__water

    def get_air(self):
        return self.__air

    def get_earth(self):
        return self.__earth

    def get__lighting(self):
        return self.__lighting

    def get__darkness(self):
        return self.__darkness

