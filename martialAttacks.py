#Sam LeCompte
#Final Project Martial Attacks

class MartialAttacks:

    def __init__(self, punches, kicks, knees, grapple):
        self.__punches = punches
        self.__kicks = kicks
        self.__knees = knees
        self.__grapple = grapple

    def get_punches(self):
        return self.__punches

    def get_kicks(self):
        return self.__kicks

    def get_knees(self):
        return self.__knees

    def get_grapple(self):
        return self.__grapple
