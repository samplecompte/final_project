#Sam LeCompte
#Final Project Weapon class

class Weapon:

    def __init__(self, piercing, slashing, bludgeoning, distance, parry, magic):
        self.__piercing = piercing
        self.__slashing = slashing
        self.__bludgeoning = bludgeoning
        self.__distance = distance
        self.__parry = parry
        self.__magic = magic

    def get_piercing(self):
        return self.__piercing

    def get_slashing(self):
        return self.__slashing

    def get_bludgeoning(self):
        return self.__bludgeoning

    def get_distance(self):
        return self.__distance

    def get_parry(self):
        return self.__parry

    def get_magic(self):
        return self.__magic
